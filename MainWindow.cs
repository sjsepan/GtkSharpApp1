using System;
using System.Threading.Tasks;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace GtkSharpApp1
{
    //Can base window on Window or its subclass ApplicationWindow
    public class MainWindow : ApplicationWindow//Window
    {
        private const string ACTION_INPROGRESS = " ...";
        private const string ACTION_DONE = " done";

        #region Controls
        [UI] private readonly ImageMenuItem _menuFileNew = null; //CS0649 apparently does not take into account [UI] attribute and how controls are wired to glade file
        [UI] private readonly ImageMenuItem _menuFileOpen = null;
        [UI] private readonly ImageMenuItem _menuFileSave = null;
        [UI] private readonly ImageMenuItem _menuFileSaveAs = null;
        [UI] private readonly ImageMenuItem _menuFilePrint = null;
        [UI] private readonly ImageMenuItem _menuFilePrintPreview = null;
        [UI] private readonly ImageMenuItem _menuFileQuit = null;
        [UI] private readonly ImageMenuItem _menuEditUndo = null;
        [UI] private readonly ImageMenuItem _menuEditRedo = null;
        [UI] private readonly ImageMenuItem _menuEditSelectAll = null;
        [UI] private readonly ImageMenuItem _menuEditCut = null;
        [UI] private readonly ImageMenuItem _menuEditCopy = null;
        [UI] private readonly ImageMenuItem _menuEditPaste = null;
        [UI] private readonly MenuItem _menuEditPasteSpecial = null;
        [UI] private readonly ImageMenuItem _menuEditDelete = null;
        [UI] private readonly ImageMenuItem _menuEditFind = null;
        [UI] private readonly ImageMenuItem _menuEditReplace = null;
        [UI] private readonly ImageMenuItem _menuEditRefresh = null;
        [UI] private readonly ImageMenuItem _menuEditPreferences = null;
        [UI] private readonly ImageMenuItem _menuHelpContents = null;
        [UI] private readonly MenuItem _menuHelpCheckForUpdates = null;
        [UI] private readonly ImageMenuItem _menuHelpAbout = null;
        [UI] private readonly ToolButton _tbFileNew = null;
        [UI] private readonly ToolButton _tbFileOpen = null;
        [UI] private readonly ToolButton _tbFileSave = null;
        [UI] private readonly ToolButton _tbFileSaveAs = null;
        [UI] private readonly ToolButton _tbFilePrint = null;
        [UI] private readonly ToolButton _tbEditUndo = null;
        [UI] private readonly ToolButton _tbEditRedo = null;
        [UI] private readonly ToolButton _tbEditCut = null;
        [UI] private readonly ToolButton _tbEditCopy = null;
        [UI] private readonly ToolButton _tbEditPaste = null;
        [UI] private readonly ToolButton _tbEditDelete = null;
        [UI] private readonly ToolButton _tbEditFind = null;
        [UI] private readonly ToolButton _tbEditReplace = null;
        [UI] private readonly ToolButton _tbEditRefresh = null;
        [UI] private readonly ToolButton _tbEditPreferences = null;
        [UI] private readonly ToolButton _tbHelpContents = null;
        [UI] private readonly Label _label1 = null;
        [UI] private readonly Button _button1 = null;
        [UI] private readonly Button _button2 = null;
        [UI] private readonly SpinButton _spinButtonResult = null;
        [UI] private readonly Label _statusMessage = null;
        [UI] private readonly Label _errorMessage = null;
        [UI] private readonly ProgressBar _progressBar = null;
        [UI] private readonly Image _pictureAction = null;
        // [UI] private readonly Image _pictureDirty = null;
        [UI] private readonly Button _buttonColor = null;
        [UI] private readonly Button _buttonFont = null;
        #endregion Controls

        private int _counter;

        public MainWindow() : this(new Builder("MainWindow.glade")) { }

        private MainWindow(Builder builder) : base(builder.GetRawOwnedObject("MainWindow"))
        {
            builder.Autoconnect(this);
            #region Events
            DeleteEvent += Window_DeleteEvent;
            _menuFileNew.Activated += MenuFileNew_Activated;
            _menuFileOpen.Activated += MenuFileOpen_Activated;
            _menuFileSave.Activated += MenuFileSave_Activated;
            _menuFileSaveAs.Activated += MenuFileSaveAs_Activated;
            _menuFilePrint.Activated += MenuFilePrintActivated;
            _menuFilePrintPreview.Activated += MenuFilePrintPreview_Activated;
            _menuFileQuit.Activated += MenuFileQuit_Activated;
            _menuEditUndo.Activated += MenuEditUndo_Activated;
            _menuEditRedo.Activated += MenuEditRedo_Activated;
            _menuEditSelectAll.Activated += MenuEditSelectAll_Activated;
            _menuEditCut.Activated += MenuEditCut_Activated;
            _menuEditCopy.Activated += MenuEditCopyActivated;
            _menuEditPaste.Activated += MenuEditPaste_Activated;
            _menuEditPasteSpecial.Activated += MenuEditPasteSpecial_Activated;
            _menuEditDelete.Activated += MenuEditDelete_Activated;
            _menuEditFind.Activated += MenuEditFind_Activated;
            _menuEditReplace.Activated += MenuEditReplace_Activated;
            _menuEditRefresh.Activated += MenuEditRefresh_Activated;
            _menuEditPreferences.Activated += MenuEditPreferences_Activated;
            _menuHelpContents.Activated += MenuHelpContents_Activated;
            _menuHelpCheckForUpdates.Activated += MenuHelpCheckForUpdates_Activated;
            _menuHelpAbout.Activated += MenuHelpAbout_Activated;
            _tbFileNew.Clicked += ToolbarFileNew_Clicked;
            _tbFileOpen.Clicked += ToolbarFileOpen_Clicked;
            _tbFileSave.Clicked += ToolbarFileSave_Clicked;
            _tbFileSaveAs.Clicked += ToolbarFileSaveAs_Clicked;
            _tbFilePrint.Clicked += ToolbarFilePrint_Clicked;
            _tbEditUndo.Clicked += ToolbarEditUndo_Clicked;
            _tbEditRedo.Clicked += ToolbarEditRedo_Clicked;
            _tbEditCut.Clicked += ToolbarEditCut_Clicked;
            _tbEditCopy.Clicked += ToolbarEditCopy_Clicked;
            _tbEditPaste.Clicked += ToolbarEditPaste_Clicked;
            _tbEditDelete.Clicked += ToolbarEditDelete_Clicked;
            _tbEditFind.Clicked += ToolbarEditFind_Clicked;
            _tbEditReplace.Clicked += ToolbarEditReplace_Clicked;
            _tbEditRefresh.Clicked += ToolbarEditRefresh_Clicked;
            _tbEditPreferences.Clicked += ToolbarEditPreferences_Clicked;
            _tbHelpContents.Clicked += ToolbarHelpContents_Clicked;
            _button1.Clicked += Button1_Clicked;
            _button2.Clicked += Button2_Clicked;
            _spinButtonResult.ValueChanged += SpinButtonResult_ValueChanged;
            _buttonColor.Clicked += ButtonColor_Clicked;
            _buttonFont.Clicked += ButtonFont_Clicked;
            #endregion Events

            //Not shown in title bar, but visible in Alt-Tab
            SetIconFromFile("./Resources/App.png");

            _statusMessage.Text = "";
            _errorMessage.Text = "";
        }

        #region Handlers
        /// <summary>
        /// handles Window_Delete and (indirectly) FileQuit_Clicked
        /// </summary>
        private void Window_DeleteEvent(object sender, DeleteEventArgs e)
        {
            bool resultDontQuit = false;

            FileQuitAction(ref resultDontQuit);
            e.RetVal = resultDontQuit;
            if (!resultDontQuit)
            {
                //clean up data model here

                Application.Quit();
            }
        }

        private async void MenuFileNew_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuFileNew.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.New); //_menuFileNew.Image.?
            await FileNewAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private /*async*/ void MenuFileOpen_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuFileOpen.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Open); //_menuFileOpen.Image.?
            /*await*/ FileOpenAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private /*async*/ void MenuFileSave_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuFileSave.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Save); //_menuFileSave.Image.?
            /*await*/ FileSaveAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private /*async*/ void MenuFileSaveAs_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuFileSaveAs.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.SaveAs); //_menuFileSaveAs.Image.?
            /*await*/ FileSaveAsAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private /*async*/ void MenuFilePrintActivated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuFilePrint.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Print); //_menuFilePrint.Image.?
            /*await*/ FilePrintAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuFilePrintPreview_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuFilePrintPreview.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.PrintPreview); //_menuFilePrintPreview.Image.?
            await FilePrintPreviewAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        /// <summary>
        /// triggers Window_Delete which handles prompting user,
        /// and cancelling or quitting
        /// </summary>
        private /*async*/ void MenuFileQuit_Activated(object sender, EventArgs a)
        {
            this.Close();
        }

        private async void MenuEditUndo_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditUndo.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Undo); //_menuEditUndo.Image.?
            await EditUndoAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditRedo_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditRedo.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Redo); //_menuEditRedo.Image.?
            await EditRedoAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditSelectAll_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditSelectAll.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.SelectAll); //_menuEditSelectAll.Image.?
            await EditSelectAllAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditCut_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditCut.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Cut); //_menuEditCut.Image.?
            await EditCutAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditCopyActivated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditCopy.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Copy); //_menuEditCopy.Image.?
            await EditCopyAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditPaste_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditPaste.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Paste); //_menuEditPaste.Image.?
            await EditPasteAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditPasteSpecial_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditPasteSpecial.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            await EditPasteSpecialAction();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditDelete_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditDelete.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Delete); //_menuEditDelete.Image.?
            await EditDeleteAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditFind_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditFind.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Find); //_menuEditFind.Image.?
            await EditFindAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditReplace_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditReplace.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.FindAndReplace); //_menuEditReplace.Image.?
            await EditReplaceAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuEditRefresh_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditRefresh.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Refresh); //_menuEditRefresh.Image.?
            await EditRefreshAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private /*async*/ void MenuEditPreferences_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuEditPreferences.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Preferences); //_menuEditPreferences.Image.?
            /*await*/ EditPreferencesAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private async void MenuHelpContents_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuHelpContents.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Help); //_menuHelpContents.Image.?
            await HelpContentsAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpCheckForUpdates_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuHelpCheckForUpdates.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            await HelpCheckForUpdatesAction();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private /*async*/ void MenuHelpAbout_Activated(object sender, EventArgs a)
        {
            _statusMessage.Text = _menuHelpAbout.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.About); //_menuHelpAbout.Image.?
										  //await DoSomething();
			HelpAboutAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarFileNew_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbFileNew.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.New); //_tbFileNew.?
            await FileNewAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private /*async*/ void ToolbarFileOpen_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbFileOpen.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Open); //_tbFileOpen.?
            /*await*/ FileOpenAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private /*async*/ void ToolbarFileSave_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbFileSave.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Save); //_tbFileSave.?
            /*await*/ FileSaveAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private /*async*/ void ToolbarFileSaveAs_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbFileSaveAs.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.SaveAs); //_tbFileSaveAs.?
            /*await*/ FileSaveAsAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private /*async*/ void ToolbarFilePrint_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbFilePrint.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Print); //_tbFilePrint.?
            /*await*/ FilePrintAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarEditUndo_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditUndo.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Undo); //_tbEditUndo.?
            await EditUndoAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarEditRedo_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditRedo.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Redo); //_tbEditRedo.?
            await EditRedoAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarEditCut_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditCut.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Cut); //_tbEditCut.?
            await EditCutAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarEditCopy_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditCopy.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Copy); //_tbEditCopy.?
            await EditCopyAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarEditPaste_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditPaste.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Paste); //_tbEditPaste.?
            await EditPasteAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarEditDelete_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditDelete.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Delete); //_tbEditDelete.?
            await EditDeleteAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarEditFind_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditFind.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Find); //_tbEditFind.?
            await EditFindAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarEditReplace_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditReplace.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.FindAndReplace); //_tbEditReplace.?
            await EditReplaceAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarEditRefresh_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditRefresh.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Refresh); //_tbEditRefresh.?
            await EditRefreshAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private /*async*/ void ToolbarEditPreferences_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbEditPreferences.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Preferences); //_tbEditPreferences.?
            /*await*/ EditPreferencesAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private async void ToolbarHelpContents_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = _tbHelpContents.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Help); //_tbHelpContents.?
            await HelpContentsAction();
            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        private void Button1_Clicked(object sender, EventArgs a)
        {
            _counter++;
            _label1.Text = "Result: " + _counter;
        }

        private void Button2_Clicked(object sender, EventArgs a)
        {
            _counter--;
            _label1.Text = "Result: " + _counter;
        }

        private void SpinButtonResult_ValueChanged(object sender, EventArgs a)
        {
            _label1.Text = "SpinButtonResult_ValueChanged:" + _spinButtonResult.Value.ToString();
        }

        private void ButtonColor_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = "Get Color" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.ColorPicker); //_tbEditDelete.?

            string errorMessage = null;
            Gdk.Color color = Gdk.Color.Zero;
            if (GetColor(ref color, ref errorMessage))
            {
                _statusMessage.Text += color.ToString();
            }
            else
            {
                _statusMessage.Text += " cancelled";
            }

            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }

        private void ButtonFont_Clicked(object sender, EventArgs a)
        {
            _statusMessage.Text = "Get Font" + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Italic); //_tbEditDelete.?

            string errorMessage = null;
            // Pango.Font font = null;
            //Note:must be set or gets null ref error after dialog closes
            Pango.FontDescription fontDescription = new( );
            if (GetFont(ref fontDescription, ref errorMessage))
            {
                _statusMessage.Text += fontDescription.ToString();
            }
            else
            {
                _statusMessage.Text += " cancelled";
            }

            StopActionIcon();
            StopProgressBar();
            _statusMessage.Text += ACTION_DONE;
        }
        #endregion Handlers

        #region Actions
        private async Task DoSomething()
        {
            for (int i = 0; i < 3; i++)
            {
                _progressBar.Pulse();
                Main.IterationDo(false);
                await Task.Delay(1000);
            }
        }

        private async Task FileNewAction()
        {
            await DoSomething();
        }
        private /*async Task*/void FileOpenAction()
        {
			try
			{
				FileChooserDialog fileChooserDialog =
                    new (
                        "Open...",
                        this,
                        FileChooserAction.Open,
                        Stock.Open,
                        ResponseType.Ok,
                        Stock.Cancel,
                        ResponseType.Cancel
                    );

				//fileChooserDialog.Modal = true;
				ResponseType response = (ResponseType)fileChooserDialog.Run();

				if (response == ResponseType.Ok)
                {
                    _statusMessage.Text += fileChooserDialog.Filename;
                }
                else if (response == ResponseType.Cancel)
                {
                    _statusMessage.Text += " cancelled ";
                }

                fileChooserDialog.Destroy();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private /*async Task*/void  FileSaveAction(bool isSaveAs=false)
        {
			try
			{
				FileChooserDialog fileChooserDialog =
                    new (
                        isSaveAs ? "Save As..." : "Save...",
                        this,
                        FileChooserAction.Save,
                        isSaveAs ? Stock.SaveAs : Stock.Save,
                        ResponseType.Ok,
                        Stock.Cancel,
                        ResponseType.Cancel
                    );

				//fileChooserDialog.Modal = true;
				ResponseType response = (ResponseType)fileChooserDialog.Run();

				if (response == ResponseType.Ok)
                {
                    _statusMessage.Text += fileChooserDialog.Filename;
                }
                else if (response == ResponseType.Cancel)
                {
                    _statusMessage.Text += " cancelled ";
                }

                fileChooserDialog.Destroy();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private /*async Task*/void  FileSaveAsAction()
        {
            //await DoSomething();
            FileSaveAction(true);
        }
        private /*async Task*/void  FilePrintAction()
        {
			try
			{
				PrintUnixDialog printDialog = new("Print...", this);
				ResponseType response = (ResponseType)printDialog.Run();

				if (response == ResponseType.Ok)
                {
                    _statusMessage.Text += printDialog.SelectedPrinter.Name;
                }
                else if (response == ResponseType.Cancel)
                {
                    _statusMessage.Text += " cancelled ";
                }

                printDialog.Destroy();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private async Task FilePrintPreviewAction()
        {
            await DoSomething();
        }

        /// <summary>
        /// called directly by Window_Delete and (indirectly) by FileQuit_Clicked
        /// </summary>
        private void FileQuitAction
        (
            ref bool resultDontQuit
        )
        {
			_statusMessage.Text = _menuFileQuit.TooltipText + ACTION_INPROGRESS;
            StartProgressBar();
            StartActionIcon(Stock.Quit);

			MessageDialog questionMessageDialog =
				new(
					this,
					DialogFlags.DestroyWithParent,
					MessageType.Question,
					ButtonsType.YesNo,
					"Are you sure you want to quit?"
				)
				{
					Title = "Quit?",
					Modal = true
				};
			ResponseType response = (ResponseType)questionMessageDialog.Run();
			questionMessageDialog.Destroy();

            if (response == ResponseType.No)
            {
                resultDontQuit = true;

                StopActionIcon();
                StopProgressBar();
                _statusMessage.Text += "cancelled";//ACTION_DONE;
            }
        }

        private async Task EditUndoAction()
        {
            await DoSomething();
        }
        private async Task EditRedoAction()
        {
            await DoSomething();
        }
        private async Task EditSelectAllAction()
        {
            await DoSomething();
        }
        private async Task EditCutAction()
        {
            await DoSomething();
        }
        private async Task EditCopyAction()
        {
            await DoSomething();
        }
        private async Task EditPasteAction()
        {
            await DoSomething();
        }
        private async Task EditPasteSpecialAction()
        {
            await DoSomething();
        }
        private async Task EditDeleteAction()
        {
            await DoSomething();
        }
        private async Task EditFindAction()
        {
            await DoSomething();
        }
        private async Task EditReplaceAction()
        {
            await DoSomething();
        }
        private async Task EditRefreshAction()
        {
            await DoSomething();
        }
        private /*async Task*/void EditPreferencesAction()
        {
			// await DoSomething();
			//do a folder-path dialog demo for preferences
			try
			{
				FileChooserDialog fileChooserDialog =
                    new (
                        "Open Folder...",
                        this,
                        FileChooserAction.SelectFolder,
                        Stock.Open,
                        ResponseType.Ok,
                        Stock.Cancel,
                        ResponseType.Cancel
                    );

				//fileChooserDialog.Modal = true;
				ResponseType response = (ResponseType)fileChooserDialog.Run();

				if (response == ResponseType.Ok)
                {
                    _statusMessage.Text += fileChooserDialog.Filename;
                }
                else if (response == ResponseType.Cancel)
                {
                    _statusMessage.Text += " cancelled ";
                }

                fileChooserDialog.Destroy();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private async Task HelpContentsAction()
        {
            await DoSomething();
        }
        private async Task HelpCheckForUpdatesAction()
        {
            await DoSomething();
        }
        private static void HelpAboutAction()
        {
			AboutDialog about =
				new()
				{
					ProgramName = "GtkSharp1 example",
					Version = "v0.3",
					Copyright = "Copyright © 2021 Stephen J Sepan",
					Comments = "Desktop GUI app prototype, on Linux, in C# / DotNet[8] / GtkSharp, using VSCode / Glade.",
					Website = "https://gitlab.com/sjsepan/GtkSharpApp1",
					Logo = new Gdk.Pixbuf("./Resources/App.png")
				};
			about.Run();
            about.Destroy();
        }
        #endregion Actions

        private void StartProgressBar()
        {
            _progressBar.Fraction = 0.33;
            _progressBar.PulseStep=0.1;
            _progressBar.Visible = true;
            _progressBar.Pulse();
            Main.IterationDo (false);
        }

        private void StopProgressBar()
        {
            _progressBar.Pulse();
            Main.IterationDo (false);
            _progressBar.Visible = false;
            _progressBar.Fraction = 0.0;
            _progressBar.PulseStep=0.0;
        }

        private void StartActionIcon(string stockIconId)
        {
            _pictureAction.IconName = stockIconId; //ex: Stock.New;
            _pictureAction.Visible = true;
        }

        private void StopActionIcon()
        {
            _pictureAction.Visible = false;
            _pictureAction.IconName = Stock.New; //_menuFileNew.Image.?
        }

        /// <summary>
        /// Get a color.
        /// </summary>
        /// <param name="color">ref Gdk.Color</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public bool GetColor
        (
            ref Gdk.Color color,
            ref string errorMessage
        )
        {
			bool returnValue;
			try
			{
				color = Gdk.Color.Zero;
				//Gdk.Color colorResponse = Gdk.Color.Zero;
				ColorChooserDialog colorChooserDialog =
                    new		(
                        "Select Color",
                        null //TODO:this
                    );

				ResponseType response = (ResponseType)colorChooserDialog.Run();
				if (response == ResponseType.Ok)
				{
					_statusMessage.Text += colorChooserDialog.Rgba.ToString();
					returnValue = true;
				}
				else if (response == ResponseType.Cancel)
				{
					_statusMessage.Text += " cancelled ";
				}

				colorChooserDialog.Destroy();
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(ex.Message);
			}
			finally
			{
				returnValue = true;
			}
			return returnValue;
        }

        /// <summary>
        /// Get a font.
        /// </summary>
        /// <param name="font">ref Pango.FontDescription</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public bool GetFont
        (
            ref Pango.FontDescription font,
            ref string errorMessage
        )
        {
			bool returnValue;
			try
			{
				//Pango.FontDescription fontResponse = null;
				// fontResponse = null;
				FontChooserDialog fontChooserDialog =
					new					(
						"Select Font",
						null //TODO:this
					);

				ResponseType response = (ResponseType)fontChooserDialog.Run();

				if (response == ResponseType.Ok)
                {
                    _statusMessage.Text += fontChooserDialog.Font;
                    returnValue = true;
                }
                else if (response == ResponseType.Cancel)
                {
                    _statusMessage.Text += " cancelled ";
                }

                fontChooserDialog.Destroy();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
            finally
            {
                returnValue = true;
            }
            return returnValue;
        }
    }
}
