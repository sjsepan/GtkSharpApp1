# Desktop GUI app prototype, on Linux, in C# / DotNet[5|6] / GtkSharp, using VSCode / Glade

![GtkSharpApp1.png](./GtkSharpApp1.png?raw=true "Screenshot")

## Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

## Instructions/example of adding GtkSharp to dotnet and creating app ui in Glade

<https://yasar-yy.medium.com/cross-platform-desktop-gui-application-using-net-core-5894b0ad89a8>

## GUI tutorial for Gtk#/C\#

<https://zetcode.com/gui/gtksharp/>

## Glade

~Glade editor 3.22.2, Gtk 3.18
