using System;
using Gtk;

namespace GtkSharpApp1
{
    static class Program
    {
        [STAThread]
        public static int Main(string[] args)
        {
			//default to fail code
			int returnValue = -1;

            try
            {
                Application.Init();

                Application app = new("org.GtkSharpApp1.GtkSharpApp1", GLib.ApplicationFlags.None);
                app.Register(GLib.Cancellable.Current);

                MainWindow win = [];
                app.AddWindow(win);

                win.Show();
                Application.Run();

                //return success code
                returnValue = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
            return returnValue;
        }
    }
}
